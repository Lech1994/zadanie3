package db.catalogs;

import java.sql.Connection;

import db.AddressRepository;
import db.PermissionRepository;
import db.PersonRepository;
import db.RepositoryCatalog;
import db.RoleRepository;
import db.UserRepository;
import db.repos.HsqlAddressRepository;
import db.repos.HsqlPermissionRepository;
import db.repos.HsqlPersonRepository;
import db.repos.HsqlRoleRepository;
import db.repos.HsqlUserRepository;

public class HsqlRepositoryCatalog implements RepositoryCatalog{

	Connection connection;
	
	public HsqlRepositoryCatalog(Connection connection) {
		this.connection = connection;
	}

	public PersonRepository people() {
		return new HsqlPersonRepository(connection);
	}


	public AddressRepository addresses() {
		return new HsqlAddressRepository(connection);
	}

	
	public RoleRepository roles() {
		return new HsqlRoleRepository(connection);
	}

	
	public PermissionRepository permissions() {
		return new HsqlPermissionRepository(connection);
	}

	
	public UserRepository users() {
		return new HsqlUserRepository(connection);
	}

}