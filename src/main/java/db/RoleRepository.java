package db;


import java.util.List;

import Domain.Role;

public interface RoleRepository extends Repository<Role> {
	public List<Role> withName(String name, PagingInfo page);
}