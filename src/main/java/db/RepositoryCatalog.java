package db;

ublic public interface RepositoryCatalog {
	
	public PersonRepository people();
	public AddressRepository addresses();
	public RoleRepository roles();
	public PermissionRepository permissions();
	public UserRepository users();
}