package db;


import java.util.List;

import pl.edu.pjatk.mpr.user.module.Address;

public interface AddressRepository extends Repository<Address> {
	public List<Address> withCountry(String country, PagingInfo page);
	public List<Address> withCity(String city, PagingInfo page);
	public List<Address> withStreet(String street, PagingInfo page);
}